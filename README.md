# Ansible Role: VirtualBox

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-virtualbox/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-virtualbox/-/commits/main)

This role installs [VirtualBox](https://www.virtualbox.org/) application on any supported host.

## TODO:

- Enable vboxweb service

## Requirements

None

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-virtualbox enmanuelmoreira.virtualbox
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    virtualbox_version: "7.1"
    virtualbox_version_minor: "0"
    virtualbox_repo_base_url: https://download.virtualbox.org/virtualbox/
    virtualbox_extpack_version: "{{ virtualbox_version }}.{{ virtualbox_version_minor }}"

    virtualbox_user: "user"
    virtualbox_create_vm_folder: true
    virtualbox_vm_dir: /home/{{ virtualbox_user }}/vms

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.virtualbox

## License

MIT / BSD
